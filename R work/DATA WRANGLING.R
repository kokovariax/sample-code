#WRITTEN BY UKACHUKWU CHRISTIAN
#ITS A PRACTICE OF DATA WRANGLING


#WE IMPORT THE GIVEN XLSX DATA FOR THE CLCULATION WE NEED
##LOADING IN THE FIRST SHEET

library(xlsx)
dataOne<-read.xlsx("A_2020.xlsx", sheetIndex = 1, header = TRUE)
head(dataOne, 2)
##LOADING THE SECOND SHEET WITH SOME DATE FIELDS
dataTwo<-read.xlsx("A_2020.xlsx", sheetIndex = 2, header = TRUE)
head(dataTwo, 2)

###LET US REVIEW OUR DATA SETS BEFOR MERGING THEM
#For the first dataframe
names(dataOne)

#And for the second data frame
names(dataTwo)

#from here you will see that there is no age column in the second data sect, hence we create an age column and at same time get the age from the dob given

today = as.Date("2020-09-01")
DateOfBirth = as.Date(dataTwo$DoB)
dataTwo$Age = round(as.numeric(difftime(today, DateOfBirth, units = "weeks")/52.25), digits = 1)
head(dataTwo,2)

#now having created the Age column ans it is situated in the column 10 of the dataframe, we have to reorder it to fit into what we want

dataTwo <- dataTwo[, c(1,2,3,4,10,5,6,7,8,9)]
head(dataTwo,2)

#Now we have reordered the columns of the second dataframe to be exactly like that of the first dataframe in index, hence we proceed with the merging

#First we confirm that the names of the data sets are well intersected

intersect(names(dataOne),names(dataTwo))



#from this you will know that our work is clean and Merging is possible

data_merged = merge(dataOne,dataTwo, all = TRUE)
head(data_merged,3) 


##Now, having evaluated merged the first two datasets fully, we proceed to producing column of index of row

data_merged$Uniq_id <- seq.int(nrow(data_merged)) 
head(data_merged,3)

#now to extract the first letter of the forename as a data set of its own
#WE WILL NEED THE stringr package to extract the first letters of each word
library(stringr)
data_merged$fore_first <- substr(data_merged$Forename,1,1)
head(data_merged$fore_first)


##and in same manner, extract the first letters of Surname
data_merged$sur_first <- substr(data_merged$Surname,1,1)
head(data_merged$sur_first)



##now the data set will now be a concatenation of the Uniq_id, fore_first, and sur_first

data_merged$identifier<- paste(data_merged$Uniq_id,data_merged$fore_first,data_merged$sur_first, sep = "")
head(data_merged$identifier)


##AFTER ACHIEVING THIS, DELETE THE THREE ROWS, Uniq_id, fore_first and sur_first from the data_merged dataframe
data_merged = subset(data_merged, select=-c(Uniq_id,fore_first,sur_first))
head(data_merged$identifier,3)


#WE FIRST LOAD IN THE THIRD SHEET INTO R AND TRY MERGING IT WITH THE ALREADY ARRANGED DATAFRAME data_merged


dataThree <-read.xlsx("A_2020.xlsx", sheetIndex = 3, header = TRUE)
by_ID <- merge(data_merged,dataThree,by.dataThree = IDNum,all = TRUE)
head(by_ID)



#LET US NOW MERGE THE FIRST DATA FRAME, data_merged WITH THE SHEET 3 DATA FRAME, dataThree, USING fulljoin on dplyr FROM THE tidyverse PACKAGE


suppressPackageStartupMessages(library(tidyverse))
dataFull = full_join(data_merged, dataThree, by = c("Forename", "Surname"))
head(dataFull)



#use and if statement to produce an age category column
dataFull$ageCat = ifelse(dataFull$Age >=65, "Pensioner",
                         ifelse(dataFull$Age >=55, "Elderly",
                                ifelse(dataFull$Age >=45, "Middle Age", 
                                       ifelse(dataFull$Age >=35, "Adult",
                                              ifelse(dataFull$Age >=25, "Young Adult",
                                                     ifelse(dataFull$Age >=18, "Youth", "Child"))))))

head(dataFull$ageCat,20)



#We extract the data and filter it by Age Category, ageCat and their criminal records, 

suppressPackageStartupMessages(library(ggplot2))
qplot(ageCat, data = dataFull,main = "CRIMINAL RECORDS BY AGE CATEGORY", xlab = "AGE CATEGORY", fill = dataFull$Criminal_Record, horiz=TRUE)



#WE CREATE A SMALLER DATASET WITH HEALTH INFORMATION AND SINCE WE ARE GOING TO EXPLORE IT, WE WILL GET RID OF ROWS WITH EMPTY VALUES


dataWithHealth <- na.omit(dataFull) 
head(dataWithHealth,3)

#we will pull out those numeric fields and crate a data frame of them, first without the health field


EXP_DATA <- dataWithHealth[c("Age","Salary","Weight", "Height")]
head(EXP_DATA,2)

#now with health data in place


EXP_DATA_2 <- dataWithHealth[c("Age","Salary","Weight", "Height", "Health")]
head(EXP_DATA_2,2)


#EXPLORING THE DATA SET WITHOUT THE HEALTH FIELD


pairs(EXP_DATA)



#LET US ALSO EXPLORE THE DATA SET WITH HEALTH DATA IN PLACE


pairs(EXP_DATA_2)


#let us look at the two fields that seem to have a trend

qplot(Age, Health, colour=Criminal_Record, data =dataWithHealth)


qplot(Age, Salary, colour=Health, data =dataWithHealth)+geom_smooth(method = 'lm',formula = y~x )

##From the above, applying linear model, we find that the health of persons depreciates with increase in age, irrespective of the salary they recieve.


#let us review the raltionship between age and salary with respect to different educational attainment


qplot(Age, Salary, colour=Education, data =dataWithHealth)+geom_smooth(method = 'lm',formula = y~x )

##from the above chart, you will see that almost everyone has Junior Cert and L7 Bachelor are more on the young populase and those with masters snd L8 Bachelors are in the median range making an almost uniform salary accross ages

##Let us further explore with a box plot to see if there are more facts to be found based on their age categories


p1 <- qplot(ageCat, Salary, data= dataWithHealth, fill=ageCat, 
            geom =c("boxplot"))
p2 <- qplot(ageCat, Salary, data= dataWithHealth, fill=ageCat, 
            geom =c("boxplot", "jitter"))
p1
p2


##From the boxplot above, we can understand that younger people tend to have more salary because their health is more stable than that of the older ones.


